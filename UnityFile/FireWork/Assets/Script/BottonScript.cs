﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottonScript : MonoBehaviour {

    enum FireWorksState
    {
        ori,
        All,
        Red,
        Green,
        Blue
    }

    FireWorksState state=FireWorksState.ori;

    public GameObject Red;
    public GameObject Green;
    public GameObject Blue;

    void Start ()
    {

    }

    void Update ()
    {
        switch (state)
        {
            default: break;

            case FireWorksState.All:
                Red.GetComponent<ParticleSystem>().Play();
                Blue.GetComponent<ParticleSystem>().Play();
                Green.GetComponent<ParticleSystem>().Play();
                break;

            case FireWorksState.Red:
                Red.GetComponent<ParticleSystem>().Play();
                break;

            case FireWorksState.Green:
                Green.GetComponent<ParticleSystem>().Play();
                break;

            case FireWorksState.Blue:
                Blue.GetComponent<ParticleSystem>().Play();
                break;
            case FireWorksState.ori:
                break;
        }
	}

    public void AllButton()
    {
        state = FireWorksState.All;
    }
    public void RedButton()
    {
        state = FireWorksState.Red;
    }
    public void GreenButton()
    {
        state = FireWorksState.Green;
    }
    public void BlueButton()
    {
        state = FireWorksState.Blue;
    }
    public void FullButton()
    {
        state = FireWorksState.ori;
    }
}
